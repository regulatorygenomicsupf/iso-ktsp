/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ktsp;

/**
 *
 * @author michal
 */
public class PairRankInfo implements Comparable<PairRankInfo> {
    Integer firstIndex;
    Integer secondIndex;
    Double score;
    Double rankScore;

    PairRankInfo(int fi, int si, double s, double rs) {
        firstIndex = fi;
        secondIndex = si;
        score = s;
        rankScore = rs;
    }

    public int compareTo(PairRankInfo other) {
        if (score.compareTo(other.score) != 0) {
            return score.compareTo(other.score);
        }
        else if (rankScore.compareTo(other.rankScore) != 0) {
            return rankScore.compareTo(other.rankScore);
        }
        else if (firstIndex.compareTo(other.firstIndex) != 0) {
            return firstIndex.compareTo(other.firstIndex);
        }
        
        else {
            return secondIndex.compareTo(other.secondIndex);
        }
    }
}
