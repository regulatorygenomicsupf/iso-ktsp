package ktsp;


import com.sun.org.apache.bcel.internal.util.ByteSequence;
import java.util.ArrayList;
import java.io.*;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.lang.NumberFormatException;

/**
 *
 * @author michal
 *
 * This is an IO class that reads and processes the main input file that contains the dataset,
 * Note that it reads each line by request, and it doesn't store all the lines that have been read before.
 */
public class FileReader {
    // The processed numeric data of the last line that has been read.
    private List<Double> currentDataLine;
    // The list of the names or ids of the samples or columns. This corresponds to the first line in tthe file.
    private List<String> sampleIdList;
    // Java classes to easily manage input from files.
    private BufferedReader br;
    private FileInputStream fis;
    private DataInputStream dis;
    // The list of gene or isoform names or ids that have already been read. These ids correspond to the first column of each row.
    // Note that this will not contain the complete list of names or ids until all the lines have been read.
    private List<String> geneIdList;
    // The number of lines that have been read.
    private int countlines;

    // the constructor of the class. This method opens the file with the specified name or path and
    // reads the first line corresponding to the sample names or ids and stores it. It also deals
    // with common IO errors.
    FileReader(String filename) {
        // initialising all the class attributes needed for reading..
        countlines = 0;
        currentDataLine = new ArrayList();
        sampleIdList = new ArrayList();
        geneIdList = new ArrayList();
        try {
            fis = new FileInputStream(filename);
        } catch (FileNotFoundException ex) {
            System.err.println("The input file " + filename + " does not exist");
            System.exit(1);
        }
        dis = new DataInputStream(fis);
        br = new BufferedReader(new InputStreamReader(dis));
        String strLine = "";
        // reading the first line.
        try {
            countlines++;
            strLine = br.readLine();
            
        } catch (IOException ex) {
            System.err.println("Unexpected error while reading the input dataset file at line "+ countlines + ". Possibly the file is corrupted");
            System.exit(1);
        }
        // spliting the line in tabs, which is the separator for all the input and storing the sample names or ids.
        String[] temp = strLine.split("\t");
        for (int i = 0; i < temp.length; i++) {
            sampleIdList.add(temp[i]);
        }
    }

    // This method returns the sample names or ids that have been read in the constructor. Because these are read at the constructor,
    // the information is available from the start throughout the life of the class.
    List<String> getSampleNames() {
        return sampleIdList;
    }

    // This method reads the next data line (all the lines except the first one are data lines) from the file and processes and
    // parses it correspondingly before returning it. Note that the first column of the line containing the gene or isoform
    // name or id is read and stored but not returned by this method (use getGeneNames after all the lines are read to retrieve
    // this information)
    List<Double> getNextLine() {
        // clearing the previous line stored.
        currentDataLine.clear();
        // reading the data line
        String strLine = "";
        boolean end = true;
        try {
            countlines++;
            end = ((strLine = br.readLine()) == null);
        } catch (IOException ex) {
            System.err.println("Unexpected error while reading the input dataset file at line "+ countlines + ". Possibly the file is corrupted");
            System.exit(1);
        }
        // if we haven't reached the end of file, we split the line using tabs as separator, store the first column as the
        // gene or isoform name or id and parse to doubles and store the other columns.
        if (!end) {
            String[] temp = strLine.split("\t");
            geneIdList.add(temp[0]);
            for (int i = 1; i < temp.length; i++) {
                try{
                    currentDataLine.add(Double.parseDouble(temp[i]));
                } catch (NumberFormatException ex) {
                    System.err.println("Unable to parse data at line " + countlines + ". Data is not a valid double.");
                    System.exit(1);
                }
            }
            // we check if the number of columns corresponds with the sample names or ids that we've read from the first line
            if (currentDataLine.size() != sampleIdList.size()) {
                System.err.println("Line " + countlines + " of numeric data in the file doesn't contain the expected number of data");
                System.exit(1);
            }
        }
        // if we have reached the end of file, the returned list will be empty.
        return currentDataLine;
    }

    // returns the gene or isoform names or ids of the lines that have been read. Note that because not all the information is
    // present until all the lines are read, it makes more sense to call this function after reading all the lines.
    List<String> getGeneNames() {
        return geneIdList;
    }
    
    

}
