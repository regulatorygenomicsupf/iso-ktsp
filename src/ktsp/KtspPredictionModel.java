
package ktsp;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author michal
 *
 * This class is merely a container for the list of pairs that are the primary result of the ktsp algorithm. Additionally,
 * it may contain info on the testing performance of the pairs with each odd k during the crossvalidation steps of the algorithm.
 */
public class KtspPredictionModel {
    // Either the kmax pairs selected in the crossvalidation steps of the algorithm or the pairs selected for the final model
    // in the final step of the algorithm.
    List<PairRankInfo> pairs = null;

    // In the crossvalidation steps of the algorithm, the performance or accuracy achieved by each k. Note that
    // the indexes of this list do not correspond to the k, because the k used are odd and increment by 2, whereas the list
    // stores the results of only those k values that are used, and the index increments by 1. That means that
    // the index corresponding to each k value is (k - 1)/2
    List<Double> testingPerformance = null;

    // The constructor initialises the lists
    KtspPredictionModel() {
        pairs = new ArrayList();
        testingPerformance = new ArrayList();
    }
}
