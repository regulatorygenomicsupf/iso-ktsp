package ktsp;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

/**
 *
 * @author michal
 *
 * This is an IO class that reads and processes the input model files. The constructor reads all the file and constructs
 * the KtspPredictionModel object and a second method returns this model object. Note that because this class
 * uses the gene or isoform names or ids that are read from the main data input file, this class should be used
 * after reading the main data input file.
 */
public class ModelFileReader {
    // // Java classes to easily manage input from files.
    private BufferedReader br;
    private FileInputStream fis;
    private DataInputStream dis;
    // the ktsp model that has been read.
    private KtspPredictionModel model;
    // the list of gene or isoform names or ids from the main data input file to match the ids from the model file and translate
    // them into indexes which are used by the KtspPredictionModel class.
    private List<String> idGene = null;

    // the constructor initialises the attributes of the class and reads, parses and checks the pairs, translating them into
    // the corresponding indexes and constructing the KtspPredictionModel object. To get the object once it is read,
    // call the getModel method. Tabs are used as separators in the pairs and each pair should be in one line. It
    // handles errors when reading the file and also checks if the ids or names in the model pairs match those of the given list.
    ModelFileReader(String filename, List<String> genes) {
        model = new KtspPredictionModel();
        idGene = genes;
        try {
            fis = new FileInputStream(filename);
        } catch (FileNotFoundException ex) {
            System.err.println("The input model file " + filename + " does not exist");
            System.exit(1);
        }
        dis = new DataInputStream(fis);
        br = new BufferedReader(new InputStreamReader(dis));
        String strLine = "";
        try {
            strLine = br.readLine();

        } catch (IOException ex) {
            System.err.println("Unexpected error while reading the input model file. Possibly the file is corrupted");
            System.exit(1);
        }
        while (strLine != null) {
            String[] temp = strLine.split("\t");
            if (temp.length != 2) {
                System.err.println("One of the lines in the input model file does not follow the expected format for model pairs");
                System.exit(1);
            }
            int first = idGene.indexOf(temp[0]);
            int second = idGene.indexOf(temp[1]);
            if (first == -1 || second == -1) {
                System.err.println("One of the ids in the input model file does not match any of the gene or isoform ids in the input data file");
                System.exit(1);
            }
            model.pairs.add(new PairRankInfo(first, second, -1, -1));
            try {
                strLine = br.readLine();

            } catch (IOException ex) {
                System.err.println("Unexpected error while reading the input model file. Possibly the file is corrupted");
                System.exit(1);
            }

        }


    }

    // this method returns the model object read in the constructor. Because the model is read in the constructor and stored
    // within the class, this method may be called at any time and as many times as needed.
    KtspPredictionModel getModel() {
        return model;    }
}
